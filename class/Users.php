<?php

class Users {
    public $mail;
    public $pswd;

    public function __construct($mail, $pswd) {
        $this->mail = $mail;
        $this->pswd = $pswd;
    }

    public function register($firstname, $lastname, $phone, $gender, $country, $town, $street, $street_number, $postalcode, $adress_complement) {

        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $test = $dbh->prepare('SELECT mail FROM users WHERE mail = ?');
        $test->execute(array($this->mail));
        $nbr = $test->rowCount();

        if ($nbr != 0) { // Si l'email est deja utilisé, le compteur va passé a 1 et sera donc différent de 0
            echo  "<div class='error_container'><div class='error' ><p>Adresse mail déjà utilisée</p></div></div>";
        }
        else {
            $pswd = password_hash($this->pswd, PASSWORD_DEFAULT);
            $BDD = new BDD();
            $dbh = $BDD->getConnection();
            $sth = $dbh->prepare('INSERT INTO users(mail, pswd, firstname, lastname, phone, gender, country, town, street, street_number, postalcode, adress_complement) 
            VALUES (:mail, :pswd, :firstname, :lastname, :phone, :gender, :country, :town, :street, :street_number, :postalcode, :adress_complement)');
            $sth->bindParam(':mail', $this->mail);
            $sth->bindParam(':pswd', $pswd);
            $sth->bindParam(':firstname', $firstname);
            $sth->bindParam(':lastname', $lastname);
            $sth->bindParam(':phone', $phone);
            $sth->bindParam(':gender', $gender);
            $sth->bindParam(':country', $country);
            $sth->bindParam(':town', $town);
            $sth->bindParam(':street', $street);
            $sth->bindParam(':street_number', $street_number);
            $sth->bindParam(':postalcode', $postalcode);
            $sth->bindParam(':adress_complement', $adress_complement);
            $sth->execute();
        }

    }

    public function login(){

        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare('SELECT * FROM users');
        $stmt->execute();
        $resultat = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($resultat as $row) {

            if ($row['mail'] == $this->mail) {

                $pass_verif = password_verify($this->pswd, $row["pswd"]);

                if ($pass_verif) {
                    $_SESSION["id"] = (int)$row["user_id"];
                    $_SESSION["mail"] = $this->mail;
                    $_SESSION["password"] = $this->pswd;
                    $_SESSION["firstname"] = $row["firstname"];
                    $_SESSION["lastname"] = $row["lastname"];
                    $_SESSION["phone"] = (int)$row["phone"];
                    $_SESSION["gender"] = $row["gender"];
                    $_SESSION["admin"] = $row["admin"];
                    $_SESSION["country"] = $row["country"];
                    $_SESSION["town"] = $row["town"];
                    $_SESSION["street"] = $row["street"];
                    $_SESSION["street_number"] = (int)$row["street_number"];
                    $_SESSION["postalcode"] = (int)$row["postalcode"];
                    $_SESSION["adress_complement"] = $row["adress_complement"];

                    header("Location: index.php?page=account");

                } else {
                    echo "<script> alert('Les mots de passe ne correspondent pas !')</script>";
                }
            }


        }
    }


    public function updateInfo($firstname, $lastname, $phone, $country, $town, $street, $street_number, $postalcode, $adress_complement){
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare('UPDATE `users` SET firstname = :firstname,  lastname = :lastname, phone = :phone, 
                   country = :country, town = :town, street = :street, street_number= :street_number, postalcode= :postalcode,
                   adress_complement= :adress_complement WHERE mail = :mail');
        $stmt->execute(array(':firstname' => $firstname, ':lastname' => $lastname, ':phone' => $phone, ':country' => $country,
            ':town' => $town, ':street' => $street, ':street_number' => $street_number, ':postalcode' => $postalcode,
            ':adress_complement' => $adress_complement, ':mail' => $this->mail));
        //$stmt->debugDumpParams();
        // Et on modifie les infos de la session en fonction
        $_SESSION["firstname"] = $firstname;
        $_SESSION["lastname"] = $lastname;
        $_SESSION["phone"] = $phone;
        $_SESSION["country"] = $country;
        $_SESSION["town"] = $town;
        $_SESSION["street"] = $street;
        $_SESSION["street_number"] = $street_number;
        $_SESSION["postalcode"] = $postalcode;
        $_SESSION["adress_complement"] = $adress_complement;
    }

    public function updateLoginInfo(){

        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $stmt = $dbh->prepare('UPDATE `users` SET mail = :email, pswd = :pswd WHERE user_id = :id');
        $pswd = password_hash($this->pswd, PASSWORD_DEFAULT);
        $stmt->execute(array(':email' => $this->mail, ':pswd' => $pswd, ':id' => $_SESSION["id"]));

        $_SESSION["mail"] = $this->mail;
        $_SESSION["password"] = $pswd;
    }
}
