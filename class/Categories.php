<?php


class Categories {
    public $category_name;

    public function __construct($category_name){
        $this->category_name = $category_name;
    }

    public function createCategory(){
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('INSERT INTO categories(category_name) VALUES (:cName)');
        $sth->bindParam(':cName', $this->category_name);
        $sth->execute();
    }

    public function deleteCat($catID){
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $req = $dbh->query("DELETE FROM categories WHERE category_ID ='$catID'");

        return $req;
    }
}
