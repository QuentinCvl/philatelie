<?php


class Panier {
    public $userId;
    public $productId;

    public function __construct($userId, $productId) {
        $this->userId = $userId;
        $this->productId = $productId;
    }

    public function save(){
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('INSERT INTO shoppingcart(user_ID, product_ID) VALUES (:userID, :productID)');
        $sth->bindParam(':userID', $this->userId);
        $sth->bindParam(':productID', $this->productId);
        $sth->execute();
    }

}