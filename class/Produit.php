<?php


class Produit{
    public $image;
    public $name;
    public $price;
    public $stock;
    public $discount;
    public $description;
    public $category;

    public function __construct($image, $name, $price, $stock, $discount, $description, $category) {
        $this->image = $image;
        $this->name = $name;
        $this->price = $price;
        $this->stock = $stock;
        $this->discount = $discount;
        $this->description = $description;
        $this->category = $category;
    }

    public function save(){
        $BDD = new BDD();
        $dbh = $BDD->getConnection();
        $sth = $dbh->prepare('INSERT INTO products(product_image, product_name, product_price, product_stock, product_discount, product_description, category) VALUES (:pImg, :pName, :pPrice, :pStock, :pDiscount, :pDescrip, :pCategory)');
        $sth->bindParam(':pImg', $this->image);
        $sth->bindParam(':pName', $this->name);
        $sth->bindParam(':pPrice', $this->price);
        $sth->bindParam(':pStock', $this->stock);
        $sth->bindParam(':pDiscount', $this->discount);
        $sth->bindParam(':pDescrip', $this->description);
        $sth->bindParam(':pCategory', $this->category);
        $sth->execute();
    }



}