<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

// Procédure pour modifier les infos du compte ( updateInfo )
if (isset($_SESSION['mail']) && isset($_POST["firstname"])) {
    $update_user = new Users($_SESSION['mail'], $_SESSION['password']);
    $update_user->updateInfo($_POST["firstname"], $_POST["lastname"], (int)$_POST["phone"], $_POST["country"], $_POST["town"],
        $_POST["street"], (int)$_POST["street_number"], (int)$_POST["postalcode"], $_POST["adress_complement"]);
}
// Procédure pour modifier les infos de connexion du compte ( updateLoginInfo )
if (isset($_SESSION['mail']) && isset($_POST["email"]) && isset($_POST["pswd"])) {

    $update_connexion_user = new Users($_SESSION['mail'], $_SESSION['password']);
    $update_connexion_user->updateLoginInfo();

}
?>
<div id="account_page">
    <div id="account_menu">
        <div id="disconnect_button_container">
            <a href="?page=disconnect"><input type="button" value="DECONNEXION" id="disconnect_button"></a>
        </div>
        <h4>Espaces Membres</h4>
        <ul>
            <li id="menu_personnal">Mes Informations Personnelles</li>
            <li id="menu_loginId">Mes Identifiants de Connexion</li>
        </ul>
        <?php
        if (isset($_SESSION['admin']) && $_SESSION['admin'] == 1) { //Ne fait apparaitre le pannel Admin que si l'utilisateur a les droits requis
            echo "<h4>Panel Administateur</h4>";
            echo "<ul>";
            //echo "<li>Gestion d'utilisateur</li>";
            echo "<li id='menu_categorie'>Gestion Catégories</li>";
            echo "<li id='menu_addProduct'>Création de Produits</li>";
            echo "<li id='menu_updateDeleteProduct'>Modification de Produits</li>";
            echo "</ul>";
        }
        ?>

    </div>

    <div id="personal_info" class="account_content">
        <h4>Mes informations personnelles</h4>
        <form method='POST' action="?page=account">
            <div class="container_input_container">
                <span>Nom : </span>
                <div class="input_container" id="">
                    <input type="text" class="account_input" name="firstname"
                           value="<?php if (isset($_SESSION["firstname"])) echo($_SESSION["firstname"]); ?>">
                </div>
            </div>
            <div class="container_input_container">
                <span>Prénom : </span>
                <div class="input_container" id="">
                    <input type="text" class="account_input" name="lastname"
                           value="<?php if (isset($_SESSION["lastname"])) echo($_SESSION["lastname"]); ?>">
                </div>
            </div>
            <div class="container_input_container">
                <span>Téléphone : </span>
                <div class="input_container" id="">
                    <input type="number" class="account_input" name="phone"
                           value="<?php if (isset($_SESSION["phone"])) echo($_SESSION["phone"]); ?>">
                </div>
            </div>
            <div class="container_input_container">
                <span>Pays : </span>
                <div class="input_container" id="">
                    <input type="text" class="account_input" name="country"
                           value="<?php if (isset($_SESSION["country"])) echo($_SESSION["country"]); ?>">
                </div>
            </div>
            <div class="container_input_container">
                <span>Code Postal : </span>
                <div class="input_container" id="">
                    <input type="number" class="account_input" name="postalcode"
                           value="<?php if (isset($_SESSION["postalcode"])) echo($_SESSION["postalcode"]); ?>">
                </div>
            </div>
            <div class="container_input_container">
                <span>Ville : </span>
                <div class="input_container" id="">
                    <input type="text" class="account_input" name="town"
                           value="<?php if (isset($_SESSION["town"])) echo($_SESSION["town"]); ?>">
                </div>
            </div>
            <div class="container_input_container">
                <span>Numéro : </span>
                <div class="input_container" id="">
                    <input type="number" class="account_input" name="street_number"
                           value="<?php if (isset($_SESSION["street_number"])) echo($_SESSION["street_number"]); ?>">
                </div>
            </div>
            <div class="container_input_container">
                <span>Rue : </span>
                <div class="input_container" id="">
                    <input type="text" class="account_input" name="street"
                           value="<?php if (isset($_SESSION["street"])) echo($_SESSION["street"]); ?>">
                </div>
            </div>
            <div class="container_input_container">
                <span>Complément d'adresse : </span>
                <div class="input_container" id="">
                    <input type="text" class="account_input" name="adress_complement"
                           value="<?php if (isset($_SESSION["adress_complement"])) echo($_SESSION["adress_complement"]); ?>">
                </div>
            </div>
            <div class="account_button_container">
                <button type="submit" class="account_button" id="peronnal_Button">Modifier les informations</button>
            </div>

        </form>
    </div>

    <div id="loginId_info" class="account_content">
        <h4>Mes Identifiants de Connexion</h4>
        <form method='POST' action="?page=account">
            <div class="input_container" id="">
                <label for="email">Adresse e-mail : </label>
                <input type="text" name="email" value="<?php if (isset($_SESSION["mail"])) echo($_SESSION["mail"]); ?>">
            </div>
            <div class="input_container" id="">
                <label for="pswd">Mot de passe : </label>
                <input type="password" name="pswd"
                       value="<?php if (isset($_SESSION["password"])) echo($_SESSION["password"]); ?>">
            </div>
            <button type="submit" class="btn" id="LoginId_Button">Modifier les informations</button>

        </form>
    </div>

    <div id="categorie_info" class="account_content">
        <?php
        // Procédure pour créé de nouvelle catégories
        if (isset($_POST["newCategorie"])) {
            $new_category = new Categories($_POST["newCategorie"]);
            $new_category->createCategory();
        }


        ?>
        <h4>Panel de Gestion des Catégories</h4>
        <form method='POST' action="?page=account">
            <h5>Créer une catégorie : </h5>
            <span>Nom de la catégorie : </span>
            <div class="input_container" id="">
                <input type="text" class="account_input" name="newCategorie" value="" autocomplete="off">
            </div>
            <button type="submit" class="btn" id="newCategorie_Button">Valider</button>
        </form>
        <h5>Supprimer une catégorie : </h5>
        <span>Nom de la catégorie : </span>
        <?php
        //Procédure pour supprimer une catégorie
        if (isset($_POST['categorySelecteur'])) {
            $deleteCategory= new Categories("");
            $req = $deleteCategory->deleteCat($_POST['categorySelecteur']);

        }
        if(isset($req) && $req){
            $reqIsTrue = true;
            $reqInfoMessage = "La Catégorie à bien était supprimé";
        }
        ?>
        <form method="post" action="?page=account">
            <select name="categorySelecteur">
                <?php
                //Va afficher toute les catégorie dans le selecteur
                $BDD = new BDD();
                $dbh = $BDD->getConnection();
                $stmt = $dbh->query('SELECT * FROM categories');

                foreach ($stmt as $row) {
                    echo '<option value="' . $row["category_ID"] . '">';
                    echo $row["category_name"];
                    echo '</option>';
                }

                ?>
            </select>
            <button type="submit" class="btn" id="deleteCategory_Button">Valider</button>
            <?php if(isset($reqIsTrue) && $reqIsTrue) echo "<p>".$reqInfoMessage."</p>" ?>
        </form>



    </div>

    <div id="product_info" class="account_content">
        <?php
        if (isset($_POST['product_name'])) {
            $new_product = new Produit($_POST['product_image'], $_POST['product_name'], $_POST['product_price'],
                $_POST['product_stock'], $_POST['product_discount'], $_POST['product_description'], $_POST['product_category']);
            $new_product->save();
        }
        ?>

        <h4>Panel de Création de Produit</h4>
        <form method='POST' action="?page=account">
            <h5>Ajouter un produit</h5>
            <span>URL de l'image : </span>
            <div class="input_container" id="">
                <input type="text" class="account_input" name="product_image" placeholder="img/exemple.jpg" required>
            </div>
            <span>Nom du produit : </span>
            <div class="input_container" id="">
                <input type="text" class="account_input" name="product_name" required>
            </div>
            <span>Prix : </span>
            <div class="input_container" id="">
                <input type="number" step="0.01" class="account_input" name="product_price"
                       placeholder="exemple : 49.99" required>
            </div>
            <span>Stock : </span>
            <div class="input_container" id="">
                <input type="number" class="account_input" name="product_stock" required>
            </div>
            <span>Remise (en %): </span>
            <div class="input_container" id="">
                <input type="number" class="account_input" name="product_discount" placeholder="exemple : 20" required>
            </div>
            <span>Description : </span>
            <div class="input_container" id="">
                <textarea class="account_input" name="product_description"></textarea>
            </div>
            <span>Choisissez la catégorie correspondante : </span>
            <select name="product_category">
                <?php
                $stmt = $dbh->query('SELECT * FROM categories');
                foreach ($stmt as $row) {
                    echo '<option value="' . $row["category_ID"] . '">';
                    echo $row["category_name"];
                    echo '</option>';
                }
                ?>
            </select>


            <button type="submit" class="btn" id="createProduct_Button">Valider</button>

        </form>
    </div>

    <div id="product_update" class="account_content">
        <h4>Panel de Modification des Produits</h4>
        <form method="post">
            <span>Produit à modifier :</span>
            <select name="productSelecteur">
                <?php
                $BDD = new BDD();
                $dbh = $BDD->getConnection();
                $stmt = $dbh->query('SELECT * FROM products');

                foreach ($stmt as $row) {
                    $selected = ($row["product_id"] == $_POST["productSelecteur"]) ? 'selected="selected"' : '';
                    echo '<option '.$selected.' value="' . $row["product_id"] . '">';
                    echo $row["product_name"];
                    echo '</option>';
                }
                ?>
            </select>
            <?php
                if (isset($_POST["confirmProduct"])) {
                    $req = $dbh->prepare('SELECT * FROM products WHERE product_id = "' . $_POST["productSelecteur"] . '"');
                    $req->execute();
                    $reqDetailProduct = $req->fetch();

                }
            ?>
            <button type="submit" class="btn" name="confirmProduct" id="confirmProduct">Valider</button>
        </form>
        <form method="post" action="?page=account">

            <span>URL de l'image : </span>
            <div class="input_container" id="">
                <input type="text" class="account_input" name="product_image" placeholder="img/exemple.jpg"
                       value="<?php if(isset($reqDetailProduct)) echo $reqDetailProduct['product_image']?>" >
            </div>
            <span>Nom du produit : </span>
            <div class="input_container" id="">
                <input type="text" class="account_input" name="product_name"
                       value="<?php if(isset($reqDetailProduct)) echo $reqDetailProduct['product_name']?>" >
            </div>
            <span>Prix : </span>
            <div class="input_container" id="">
                <input type="number" step="0.01" class="account_input" name="product_price" placeholder="exemple : 49.99"
                       value="<?php if(isset($reqDetailProduct)) echo $reqDetailProduct['product_price']?>" >
            </div>
            <span>Stock : </span>
            <div class="input_container" id="">
                <input type="number" class="account_input" name="product_stock"
                       value="<?php if(isset($reqDetailProduct)) echo $reqDetailProduct['product_stock']?>">
            </div>
            <span>Remise (en %): </span>
            <div class="input_container" id="">
                <input type="number" class="account_input" name="product_discount" placeholder="exemple : 20"
                       value="<?php if(isset($reqDetailProduct)) echo $reqDetailProduct['product_discount']?>">
            </div>
            <span>Description : </span>
            <div class="input_container" id="">
                <textarea class="account_input" name="product_description"
                          value=""><?php if(isset($reqDetailProduct)) echo $reqDetailProduct['product_description']?></textarea>
            </div>

        </form>
    </div>
</div>


<script src="js/account.js"></script>

