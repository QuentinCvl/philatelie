<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Phil'Italia, spécialiste des timbres italiens</title>
    <!-- CSS Global -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="stylesheet" type="text/css" href="css/account.css">
    <link rel="stylesheet" type="text/css" href="css/stamp.css">
    <link rel="stylesheet" type="text/css" href="css/shoppingCart.css">
    <!-- Script Iconify -->
    <script src="https://code.iconify.design/1/1.0.3/iconify.min.js"></script>
    <!-- JQueries-->
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

</head>
<body>
<header>
    <div id="topMenu">
        <div id="logo">
            <a href="index.php"><img src="img/LogoPhi.png" alt="logo du site"></a>
        </div>

        <?php
        if (isset($_SESSION['mail'])) {
            echo("<p class='welcome'>Bonjour " . $_SESSION["lastname"] . "</p>");
        }
        ?>

        <!--<div id="searchbar"> MAYBE LATER
            <input type="text" placeholder="Rechercher">
            <span class="iconify loop" data-icon="entypo:magnifying-glass" data-inline="false"></span>
        </div> -->
        <div id="account_shopcart">
            <div id="account">
                <a href="?page=login"><span class="iconify" data-inline="false"
                                            data-icon="dashicons:admin-users"></span></a>
            </div>
            <div id="shopcart">
                <a href="?page=shoppingCart"><span class="iconify" data-inline="false" data-icon="dashicons:cart"></span></a>
                <span id="count">
                    <?php
                    if (isset($_SESSION["id"])) {
                        $BDD = new BDD();
                        $dbh = $BDD->getConnection();
                        $stmt = $dbh->query('SELECT * FROM shoppingcart WHERE user_ID="' . $_SESSION["id"] . '"');
                        $count = $stmt->rowCount();
                        echo $count;
                    } else echo "0";
                    ?>
                </span>
            </div>
        </div>
    </div>
    <div id="menu">
        <nav>
            <a href="index.php?page=stamp">Catalogue des Timbres</a></li>
            <a href="index.php?page=materials">Matériels de Philatélie</a></li>
        </nav>

    </div>

</header>
<main>
