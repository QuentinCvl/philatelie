<?php 
require_once("autoload.php");

require_once("header.php");

if(isset ($_GET['page']) && file_exists($_GET['page'].'.php')) {
    require_once($_GET['page'].'.php');
} else {
    require_once("acceuil.php");
}

require_once("footer.php");


?>