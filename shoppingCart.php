<?php
if (isset($_SESSION["id"])) {
    $BDD = new BDD();
    $dbh = $BDD->getConnection();
    $stmt = $dbh->query('SELECT * FROM shoppingcart WHERE user_ID="' . $_SESSION["id"] . '"');
    $nbrProduct = $stmt->rowCount();


} else {
    header("Location: index.php?page=acceuil");
}
if (isset($_POST['deleteToCart'])) {
    $del = $dbh->prepare('DELETE FROM shoppingcart WHERE id = :id');
    $del->execute(array(':id' => $_POST['deleteToCart']));

    header("Location: index.php?page=shoppingCart");
}

?>

<div id="shoppingCart">
    <form method="post" action="?page=shoppingCart" id="shoppingCart_products">
        <?php

        $totalPrice = 0;
        foreach ($stmt as $row) {
            $req = $dbh->query('SELECT * FROM products WHERE product_id="' . $row["product_ID"] . '"');
            $shopping_cart_ID = $row['id'];
            foreach ($req as $item) {
                echo '<div class="items">';
                echo '<div class="items_img">';
                echo '<img src="' . $item["product_image"] . '">';
                echo '</div>';

                echo '<div class="items_description">';
                echo '<h3 class="items_name">' . $item["product_name"] . '</h3>';
                echo '<p class="items_price">' . $item["product_price"] . ' € TTC</p>';
                echo '<button type="submit" class="items_button" name="deleteToCart" value="' . $shopping_cart_ID . '">';
                echo 'SUPPRIMER DU PANIER';
                echo '</button>';
                echo '</div>';
                echo '</div>';
                $totalPrice += $item['product_price'];
            }

        }

        ?>
    </form>
    <div id="shoppingCart_menu">
    <h4>Panier</h4>
        <div class="menu_content" id="">
            <span>Nombre d'article : <em class="bold"><?php echo $nbrProduct; ?></em></span>
        </div>
        <div class="menu_content" id="PriceCount">
            <span>Total de votre panier : <em class="bold"><?php echo $totalPrice ?> € TTC</em></span>
        </div>
    </div>
</div>
