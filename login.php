<?php
//Si deja connecté, redirige vers la page account
if (isset($_SESSION['mail'])) {
    header('Location: index.php?page=account');
}

// Procédure pour créé les comptes ( sign up )
if ((isset($_POST['pswd1']) && isset($_POST['pswd2'])) && ($_POST['pswd1'] === $_POST['pswd2'])) {
    // Si les champs pswd sont rempli et qu'ils sont identiques
    $new_user = new Users($_POST['email'], $_POST['pswd1']);

    $new_user->register($_POST['firstname'], $_POST['lastname'], (int)$_POST['phone'], $_POST['gender'], $_POST['country'],
        $_POST['town'], $_POST['street'], (int)$_POST['street_number'], (int)$_POST['postalcode'],  $_POST['adress_complement']);
}
// Procédure pour se connecter ( sign in )
if (isset($_POST['signup_email']) && isset($_POST['signup_pswd']) ) {

    $new_user = new Users($_POST['signup_email'], $_POST['signup_pswd']);
    $new_user->login();
}

?>
<div id="login_container">
    <div class="login-popup" id="login">
        <form method='POST' action="?page=login" class="form-container">
            <h4>Connexion</h4>
            <div class="login_input">
                <input type="text" placeholder="Adresse e-mail" name="signup_email" required>
            </div>
            <div class="login_input">
                <input type="password" placeholder="Mot de passe" name="signup_pswd" required>
            </div>
            <button type="submit" class="btn">Login</button>
        </form>
    </div>

    <div class="login-popup" id="register">
        <form method='POST' action="?page=login" class="form-container">
            <h4>Inscription</h4>
            <div class="login_input" id="firstElement">
                <input type="text" placeholder="Adresse e-mail*" name="email" required>
                <input type="password" placeholder="Mot de passe*" name="pswd1" id="pswd1" required>
                <input type="password" placeholder="Comfirmation du mot de passe*" name="pswd2" id="pswd2"
                       onblur="verifPswd()" required>
            </div>
            <div class="login_radio">
                <input type="radio" id="homme" name="gender" value="M" checked>
                <label for="femme">Homme</label>

                <input type="radio" id="femme" name="gender" value="F">
                <label for="femme">Femme</label>
            </div>
            <div class="login_input middle_size">
                <input type="text" placeholder="Nom*" name="firstname" required>
                <input type="text" placeholder="Prénom*" name="lastname" required>
            </div>
            <div class="login_input">
                <input type="text" placeholder="Numéro de téléphone*" name="phone" required>
                <input type="text" placeholder="Pays*" name="country" required>
            </div>
            <div class="login_input middle_size">
                <input type="text" placeholder="Code Postal*" name="postalcode" required>
                <input type="text" placeholder="Ville*" name="town" required>
            </div>
            <div class="login_input middle_size">
                <input type="text" placeholder="N°*" name="street_number" required style="width: 19%">
                <input type="text" placeholder="Rue*" name="street" required style="width: 80%">
            </div>
            <div class="login_input">
                <input type="text" placeholder="Complément d'adresse" name="adress_complement">
            </div>
            <button type="submit" class="btn" id="registerButton">Valider</button>

        </form>
    </div>
</div>
<script src="js/login.js"></script>
