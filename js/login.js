function verifPswd() {
    var parent = document.getElementById("firstElement");
    var pswd1 = document.getElementById("pswd1");
    var pswd2 = document.getElementById("pswd2");
    if (pswd1.value !== pswd2.value) {
        var errorPswd = document.createElement("p");
        errorPswd.textContent= "Les mots de passe ne correspondent pas";
        errorPswd.className= "error";
        parent.appendChild(errorPswd);

        var button = document.getElementById("registerButton");
        button.disabled= "true";
        button.className= "disabledButton";
    } else {
        parent.removeChild(parent.lastChild);
        var button = document.getElementById("registerButton");
        button.removeAttribute("disabled");
        button.classList.remove("disabledButton");
        button.className="btn";
    }
}
$('.error_container').click(function () {
    this.style.display= 'none';
})

/*
error.addEventListener('click', function () {

})*/
