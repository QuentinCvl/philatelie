/* Element cliquable du menu */
var menu_personnal = document.getElementById('menu_personnal');
var menu_loginId = document.getElementById('menu_loginId');
var menu_categorie = document.getElementById('menu_categorie');
var menu_addProduct = document.getElementById('menu_addProduct');
var menu_updateDeleteProduct = document.getElementById('menu_updateDeleteProduct');
/* Rubrique associé */
var personal_info = document.getElementById('personal_info');
var loginId_info = document.getElementById('loginId_info');
var categorie_info = document.getElementById('categorie_info');
var product_info = document.getElementById('product_info');
var product_update = document.getElementById('product_update');

menu_personnal.addEventListener('click', function () {
    personal_info.style.display="block";
    loginId_info.style.display= "none";
    categorie_info.style.display= "none";
    product_info.style.display= "none";
    product_update.style.display= "none";
    let url = window.location.href.split('#');
    window.location.href=url[0]+'#userInfo';

});

menu_loginId.addEventListener('click', function () {
    loginId_info.style.display= "block";
    personal_info.style.display= "none";
    categorie_info.style.display= "none";
    product_info.style.display= "none";
    product_update.style.display= "none";
    let url = window.location.href.split('#');
    window.location.href=url[0]+'#connexionInfo';
});

menu_categorie.addEventListener('click', function () {
    categorie_info.style.display= "block";
    loginId_info.style.display= "none";
    personal_info.style.display= "none";
    product_info.style.display= "none";
    product_update.style.display= "none";
    let url = window.location.href.split('#');
    window.location.href=url[0]+'#category';
    console.log(url);
});

menu_addProduct.addEventListener('click', function () {
    product_info.style.display= "block";
    loginId_info.style.display= "none";
    personal_info.style.display= "none";
    categorie_info.style.display= "none";
    product_update.style.display= "none";
    let url = window.location.href.split('#');
    let a = window.location.href=url[0]+'#addproduct';
    console.log(a);
});

menu_updateDeleteProduct.addEventListener('click', function () {
    product_update.style.display= "block";
    product_info.style.display= "none";
    loginId_info.style.display= "none";
    personal_info.style.display= "none";
    categorie_info.style.display= "none";
    let url = window.location.href.split('#');
    window.location.href=url[0]+'#update';
});
window.addEventListener('DOMContentLoaded',function(){
    let url = window.location.href.split('#');
    console.log(url[0]);
    console.log(url[1]);
    if(url.length>1){
        if(url[1] == 'update'){
            menu_updateDeleteProduct.click()
        }
        if(url[1] == 'addproduct'){
            menu_addProduct.click()
        }
        if(url[1] == 'category'){
            console.log("Redirection vers #category")
            menu_categorie.click()
            console.log(url[0]);
            console.log(url[1]);

        }
        if(url[1] == 'connexionInfo'){
            menu_loginId.click()
        }
        if(url[1] == 'userInfo'){
            menu_personnal.click()
        }
    }
})
