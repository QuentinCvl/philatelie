Version fonctionnel sur Master
Nouvelle fonctionnalité (poo) sur develop

Ce qui fonctionne : 

- Creation de compte
- Connexion
- Ajout d'article au panier
- Suppression d'article du panier
- Ajout dynamiquement les produits par rapport à la bdd
- Creation de nouveau produit / catégorie depuis le site
- Suppression de Catégorie

Ce qui ne fonctionne pas : 
- Modifier les produits deja existant depuis le site
- Suppression de produit