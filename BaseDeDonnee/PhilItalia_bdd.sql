-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: PhilItalia
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `PhilItalia`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `PhilItalia` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `PhilItalia`;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `category_ID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(60) CHARACTER SET utf8mb4 NOT NULL,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Italien','2020-02-01 12:41:59'),(12,'test','2020-02-07 13:00:27'),(13,'lala','2020-02-07 13:00:42');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_image` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `product_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `product_price` decimal(25,2) NOT NULL DEFAULT '0.00',
  `product_stock` smallint(6) NOT NULL DEFAULT '0',
  `product_discount` tinyint(100) NOT NULL DEFAULT '0',
  `product_description` text CHARACTER SET utf8mb4,
  `category` int(11) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'img/n537_50e.png','NÂ° 537 Timbre Italie',50.00,5,0,'Timbre Italien numÃ©ro 537, etc ...',1),(2,'img/n589_45e.jpg','NÂ° 589 Timbre Italie',45.00,10,0,'Timbre Italien numÃ©ro 589, etc ...',1),(3,'img/Collection1860-1980_4730e.jpg','Collection 1860 / 1980',3784.00,1,0,'Collection de timbre italien entre 1860 et 1980',1),(4,'img/n3693_2.5e.jpg','NÂ° 3693 Timbre Italie',2.50,15,0,'Timbre Italien numÃ©ro 3693, etc ...',1),(5,'img/n3719_2.5e.jpg','NÂ° 3719',2.50,45,0,'Timbre Italien numÃ©ro 3719, etc ...',1),(6,'img/n538_25e.jpg','NÂ° 538',25.00,0,20,'Timbre numÃ©ro 538, provenance italie',1);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shoppingcart`
--

DROP TABLE IF EXISTS `shoppingcart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shoppingcart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_ID` int(11) NOT NULL,
  `product_ID` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shoppingcart`
--

LOCK TABLES `shoppingcart` WRITE;
/*!40000 ALTER TABLE `shoppingcart` DISABLE KEYS */;
/*!40000 ALTER TABLE `shoppingcart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mail` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `pswd` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `firstname` varchar(45) CHARACTER SET utf8mb4 NOT NULL,
  `lastname` varchar(45) CHARACTER SET utf8mb4 NOT NULL,
  `phone` char(16) CHARACTER SET utf8mb4 NOT NULL,
  `gender` char(1) CHARACTER SET utf8mb4 NOT NULL,
  `admin` tinyint(4) DEFAULT '0',
  `country` varchar(45) CHARACTER SET utf8mb4 NOT NULL,
  `town` varchar(45) CHARACTER SET utf8mb4 NOT NULL,
  `street` varchar(45) CHARACTER SET utf8mb4 NOT NULL,
  `street_number` smallint(6) NOT NULL,
  `postalcode` int(11) NOT NULL,
  `adress_complement` tinytext CHARACTER SET utf8mb4,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (12,'user@user.fr','$2y$10$DWh8kKvE08Qb0.gZbgmhQOwPxiuk9WZBq23WmqAhVngywBZuDWC0e','User','User','4586210567','M',0,'User\'sLand','egzhgfg','regqgg',85,45256,'qzggqg'),(14,'admin@admin.fr','$2y$10$ACHyyub/gT2Yk3ScUNZObupfD57.7YF7PoI.C6OwOM2.yDyTyAxYu','Cuvelier','Quentin','9856475','M',1,'France','Valenciennes','Place d\'armes',1,59300,'Entreprise fictive - Compte Administrateur'),(17,'Jean.Michel@hotmail.fr','$2y$10$.I/ZzQTz7pT8Kgxj4PgMcO80e1vvGEEPquD/kXFfcezurO1qIIxhW','Dupont','Jean-Michel','612523794','M',0,'France','Valenciennes','rue de Famars',96,59300,'Appart B7, Etages 4'),(19,'test@hotmail.fr','$2y$10$xNll9xN0w3ZXhvRE1lmgvuCOsd57QDSbn4HDSZZJiNN1RfUNQwwG.','fcgvbhj','gvbhnj','512541','M',0,'gvbhgbh','fvgbhjnbh','tgyhjvgyu',652,84512,'  jfjr  ejzkjz $$\' :;zina 796'),(20,'charle@dd.fr','$2y$10$J50CCHRD4rGfLq38en3GA.GUK6i7a5HJ102Kg4uaFLk06WcOEESXy','Charle','Xavier','0','M',0,'inconnu','inconnu','inconnu',0,0,'inconnu'),(22,'truc@truc.fr','$2y$10$XWt7gquCFs.BnGJ0sbLoHexw5YZzF.KhgbLNpm61R9ZUpDXUsdTYW','Nom','Prenom','853453645','M',0,'Pays','Ville','Rue',5452,51115,'Complement');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-16 11:40:43
