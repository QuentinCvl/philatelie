<?php
$BDD = new BDD();
$dbh = $BDD->getConnection();
$stmt = $dbh->query('SELECT * FROM products WHERE product_stock > 0 ORDER BY rand()');


?>
<div id="stampCatalogue">
    <div id="stampMenu">
        <div id="stampMenuContent">
            <h5>Trier par : </h5>
            <div class="radio_container">
                <input type="radio" id="radio_category" name="drone" value="category" checked>
                <label for="radio_category">Catégorie</label>
            </div>

            <div class="radio_container">
                <input type="radio" id="radio_ascendingPrice" name="drone" value="product_price">
                <label for="radio_ascendingPrice">Prix Croissant</label>
            </div>

            <div class="radio_container">
                <input type="radio" id="radio_decreasingPrice" name="drone" value="product_price">
                <label for="radio_decreasingPrice">Prix Décroissant</label>
            </div>

            <div class="radio_container">
                <input type="radio" id="radio_ascendingPrice" name="drone" value="product_stock">
                <label for="radio_ascendingPrice">Disponible</label>
            </div>

            <div class="radio_container">
                <input type="radio" id="radio_ascendingPrice" name="drone" value="product_discount">
                <label for="radio_ascendingPrice">Promo</label>
            </div>
        </div>
    </div>

    <form method="post" action="?page=stamp" id="product_view">
    <?php
        if(isset($_POST['addToCart'])) {
            $new_cart = new Panier($_SESSION['id'], $_POST['addToCart']);
            $new_cart->save();
        }
    ?>
            <?php

            foreach ($stmt as $row) {

                    echo '<div class="items">';
                    echo '<div class="items_img">';
                    echo '<img src="' . $row["product_image"] . '">';
                    echo '</div>';

                    echo '<div class="items_description">';
                    echo '<h3 class="items_name">' . $row["product_name"] . '</h3>';
                    echo '<p class="items_price">' . $row["product_price"] . ' € TTC</p>';
                    echo '<button type="submit" class="items_button" name="addToCart" value="' . $row["product_id"] . '">';
                    echo 'AJOUTER AU PANIER';
                    echo '</button>';
                    echo '</div>';
                    echo '</div>';

            }
            ?>

    </form>
</div>
