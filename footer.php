</main>
<footer>
    <div id="allcontent">
        <div id="footerLogo">
            <img src="img/LogoPhi.png">
            <label><p>© 2020 all rights reserved Phil’Italia</p></label>
        </div>
        <div id="follow">
            <h4>SUIVEZ-NOUS</h4>
            <div id="icon">
                <span class="iconify footerIcon" data-icon="logos:facebook" data-inline="false"></span>
                 <span class="iconify footerIcon" data-icon="logos:twitter" data-inline="false"></span>
                <span class="iconify footerIcon" data-icon="logos:pinterest" data-inline="false"></span>
            </div>
        </div>
        <div id="aboutUs">
            <h4>A PROPOS</h4>
            <p>Mentions légales</p>
            <p>Politique de confidentialité</p>
            <p>Conditions générales d'utilisation</p>
            <p>Conditions générales de vente</p>
        </div>
        <div id="other">
            <h4>AUTRES</h4>
            <p>Plan du site</p>
            <p>FAQ</p>
        </div>
    </div>
</footer>
<!-- Jquery -->
<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous">
</script>
<script href="js/script.js"></script>
</body>
</html>