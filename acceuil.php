<?php
$BDD = new BDD();
$dbh = $BDD->getConnection();
$stmt = $dbh->query('SELECT * FROM products WHERE product_stock > 0 ORDER BY rand() LIMIT 3');

?>

<div id="bandeau">
    <img src="img/italie1.jpg">
    <div id="bandeau_text">
        <h1>Une sélection de timbres italiens pour compléter votre collection</h1>
    </div>
</div>
<div id="selection">
    <h2>Notre sélection du moment</h2>
    <form method="post" action="?page=acceuil" id="selection_container">
        <?php
        if(isset($_POST['addToCart'])) {
            $new_cart = new Panier($_SESSION['id'], $_POST['addToCart']);
            $new_cart->save();
        }
        ?>
        <?php
            foreach ($stmt as $row) {
                echo '<div class="items">';
                echo '<div class="items_img">';
                echo '<img src="'.$row["product_image"].'">';
                echo '</div>';

                echo '<div class="items_description">';
                echo '<h3 class="items_name">'.$row["product_name"].'</h3>';
                echo  '<p class="items_price">'.$row["product_price"].' € TTC</p>';
                echo '<button type="submit" class="items_button" name="addToCart" value="' . $row["product_id"] . '">';
                echo 'AJOUTER AU PANIER';
                echo '</button>';
                echo '</div>';
                echo '</div>';
            }
        ?>
    </form>
</div>
